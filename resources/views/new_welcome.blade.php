<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>G Construction LLC - Connecticut / NY</title>
    <link rel="shortcut icon" href="images/icone.png"/>
    <link href="css/estilo.css" rel="stylesheet" type="text/css" media="all"/>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-32744953-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>

<body>
<div class="logo">
    <div class="iconface">
        <ul>
            <li><a href="https://www.facebook.com/gconstructionllc/" target="_blank">Facebook</a></li>
        </ul>
    </div>
    <div class="tel">
        <ul>
            <li><a href="tel:+14752047002">Phone Number</a></li>
        </ul>
    </div>
    <div class="iconinsta">
        <ul>
            <li><a href="https://www.instagram.com/g_construction_llc/" target="_blank">Instagram</a></li>
        </ul>
    </div>
    <div class="youtubeicon">
        <ul>
            <li><a href="http://www.youtube.com/" target="_blank">YouTube</a></li>
        </ul>
    </div>
    <div class="email">
        <ul>
            <li><a href="mailto:contact@gconstructionllc.com">e-mail</a></li>
        </ul>
    </div>
    <div class="zap">
        <ul>
            <li><a href="https://wa.me/+14752047002" target="_blank">WhatsApp</a></li>
        </ul>
    </div>


</div>


</body>
</html>