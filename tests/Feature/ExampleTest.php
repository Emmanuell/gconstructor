<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mail\ContactEmail;
use App\User;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testSendEmail(){

        $user = new User();
        $user->email = 'emmanuellnogmiq@hotmail.com';
        \Mail::to($user)->send(new ContactEmail);
        $this->assertEquals(true, true);

    }
}
