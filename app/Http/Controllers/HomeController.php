<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function sendEmail()
    {
        Mail::send(new ContactEmail);
    }


}
